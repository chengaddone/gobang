"""游戏中的常量"""
# 棋盘与程序边框的padding值
SPACE = 45
# 每个格子的宽度
CELL_SIZE = 30
# 棋盘中格子的数量
CELL_NUM = 15
# 游戏窗口大小
GRID_SIZE = SPACE * 2 + CELL_NUM * (CELL_SIZE - 1)
SPACE_X = GRID_SIZE + 100
SPACE_Y = GRID_SIZE
# 棋盘中有9个黑色小圆点，坐标
C_LOC = ((7, 7), (3, 3), (11, 3), (3, 11),
         (11, 11), (7, 3), (3, 7), (11, 7), (7, 11))
