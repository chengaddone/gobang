import pygame
from .constants import CELL_NUM, CELL_SIZE, SPACE, C_LOC


def draw_chessboard(screen: object, color: tuple, chess_arr: list) -> None:
    """
    绘制棋盘的线框和棋子
    :params screen: 当前的窗口对象
    :params color: 棋盘的线条颜色，值为颜色的rgb值，用元组传入
    :params chess_arr: 棋盘当前的数组
    """
    # 绘制游戏界面中的线条
    for i in range(CELL_NUM):
        width = 3 if i == 0 or i == CELL_NUM - 1 else 1
        pygame.draw.line(screen, color, (i*CELL_SIZE+SPACE, SPACE),
                         (i*CELL_SIZE+SPACE, SPACE+(CELL_NUM-1)*CELL_SIZE), width)
        pygame.draw.line(screen, color, (SPACE, SPACE+i*CELL_SIZE),
                         (SPACE+(CELL_NUM-1)*CELL_SIZE, SPACE+i*CELL_SIZE), width)
    # 绘制棋盘中心的小实心和四周的小实心
    for x, y in C_LOC:
        pygame.draw.circle(screen, color,
                           (SPACE+x*CELL_SIZE, SPACE+y*CELL_SIZE), 4, 4)
    # 绘制当前棋盘上的棋子
    for x, y, c in chess_arr:
        chess_color = (30, 30, 30) if c == 1 else (205, 205, 205)
        pygame.draw.circle(screen, chess_color,
                           (x*CELL_SIZE+SPACE, y*CELL_SIZE+SPACE), 12, 12)


def mouse_operation(game_state: int, chess_arr: list, flag: int) -> tuple:
    """
    玩家鼠标落子操作
    :params game_state: 当前的游戏状态
    :params chess_arr: 当前的棋盘数组
    :params flag: 当前的棋子
    :return: 返回点击之后游戏的状态和棋子flag组成的元组
    """
    if game_state != 1:
        # 判别游戏状态，如果不是1，则不进行操作
        return (game_state, flag)
    # 获取当前鼠标点击的坐标
    x, y = pygame.mouse.get_pos()
    # 将坐标转换为棋盘二维数组坐标
    x = int(round((x-SPACE)*1.0/CELL_SIZE))
    y = int(round((y-SPACE)*1.0/CELL_SIZE))
    if 0 <= x < CELL_NUM and 0 <= y < CELL_NUM and (x, y, 1) not in chess_arr and (x, y, 2) not in chess_arr:
        chess_arr.append((x, y, flag))
        if check_win(chess_arr, flag):
            # 如果落子后判断某一方胜利，则停止游戏，返回当前的游戏状态和棋子flag
            return (2 if flag == 1 else 3, flag)
        else:
            # 否则切换棋子，继续游戏
            return (game_state, 2 if flag == 1 else 1)
    else:
        return (game_state, flag)


def check_win(chess_arr: list, flag: int) -> bool:
    """
    判断当前棋盘上，某一个颜色的玩家是否获胜
    :param chess_arr: 棋盘的二维数组
    :param flag: 棋子的颜色标识
    :return: 当前棋子是否获胜
    """
    m = [[0]*CELL_NUM for _ in range(CELL_NUM)]
    for x, y, c in chess_arr:
        if c == flag:
            m[y][x] = 1  # 上面有棋则标1
    lx = chess_arr[-1][0]  # 最后一个子的x
    ly = chess_arr[-1][1]  # 最后一个子的y
    # 4个方向数组,往左＋往右、往上＋往下、往左上＋往右下、往左下＋往右上，4组判断方向
    dire_arr = [[(-1, 0), (1, 0)], [(0, -1), (0, 1)],
                [(-1, -1), (1, 1)], [(-1, 1), (1, -1)]]

    for dire1, dire2 in dire_arr:
        num1 = get_num(lx, ly, dire1, m)
        num2 = get_num(lx, ly, dire2, m)
        if num1 + num2 >= 4:
            return True
    return False


def get_num(x: int, y: int, dire: tuple, board: list) -> int:
    """
    获取某一个方向上棋子连续出现的次数
    :param x: 目标棋子的x坐标
    :param y: 目标棋子的y坐标
    :param dire: 方向坐标，代表往哪个方向判断
    :param board: 棋盘坐标数组，二维
    :return: 返回连续棋子出现的次数
    """
    t_x, t_y = x, y
    dire_x, dire_y = dire
    ret = 0
    while True:
        t_x += dire_x
        t_y += dire_y
        if t_x < 0 or t_x >= CELL_NUM or t_y < 0 or t_y >= CELL_NUM or board[t_y][t_x] == 0:
            return ret
        ret += 1


def draw_script(flag: int, screen, game_state: int) -> None:
    """
    绘制棋盘中的文字
    :param flag: 当前棋子标识
    :param screen: 屏幕对象
    :param game_state: 当前的游戏状态
    """
    # 绘制棋盘右侧的说明文字
    text = "黑" if flag == 1 else "白"
    show_text(screen, (480, 50), "当前棋子："+text,
              (223, 223, 223), False, 18, False)
    # 绘制游戏结束时的提示文字
    if game_state == 2:
        show_text(screen, (200, 200), "黑子胜利",
                  (210, 210, 0), False, 30, False)
    if game_state == 3:
        show_text(screen, (200, 200), "白子胜利",
                  (210, 210, 0), False, 30, False)


def draw_mouse_indicator(screen: object, game_state: int) -> None:
    """绘制鼠标指针
    :param screen: 屏幕对象
    :param game_state: 当前的游戏状态
    """
    if game_state == 1:
        # 游戏进行状态才绘制指针
        x, y = pygame.mouse.get_pos()
        x = int(round((x-SPACE)*1.0/CELL_SIZE))
        y = int(round((y-SPACE)*1.0/CELL_SIZE))
        if 0 <= x <= CELL_NUM and 0 <= y <= CELL_NUM:
            pygame.draw.line(screen, (255, 33, 56), (x*CELL_SIZE+SPACE-int(CELL_SIZE/2), y*CELL_SIZE+SPACE-int(
                CELL_SIZE/2)), (x*CELL_SIZE+SPACE-int(CELL_SIZE/2), y*CELL_SIZE+SPACE+int(CELL_SIZE/2)), 1)
            pygame.draw.line(screen, (255, 33, 56), (x*CELL_SIZE+SPACE-int(CELL_SIZE/2), y*CELL_SIZE+SPACE-int(
                CELL_SIZE/2)), (x*CELL_SIZE+SPACE+int(CELL_SIZE/2), y*CELL_SIZE+SPACE-int(CELL_SIZE/2)), 1)
            pygame.draw.line(screen, (255, 33, 56), (x*CELL_SIZE+SPACE+int(CELL_SIZE/2), y*CELL_SIZE+SPACE-int(
                CELL_SIZE/2)), (x*CELL_SIZE+SPACE+int(CELL_SIZE/2), y*CELL_SIZE+SPACE+int(CELL_SIZE/2)), 1)
            pygame.draw.line(screen, (255, 33, 56), (x*CELL_SIZE+SPACE-int(CELL_SIZE/2), y*CELL_SIZE+SPACE+int(
                CELL_SIZE/2)), (x*CELL_SIZE+SPACE+int(CELL_SIZE/2), y*CELL_SIZE+SPACE+int(CELL_SIZE/2)), 1)


def show_text(screen: object, pos: tuple, text: str, color: tuple, font_bold=False, font_size=60, font_italic=False):
    """
    向屏幕中添加文字
    :param screen: 当前的屏幕对象
    :param pos: 文字显示的坐标
    :param text: 文字显示内容
    :param color: 颜色的rgb值
    :param font_bold: 是否加粗
    :param font_size: 字体大小
    :param font_italic: 是否斜体
    """
    # 获取系统字体，并设置大小
    cur_font = pygame.font.SysFont("SimHei", font_size)
    # 设置是否加粗
    cur_font.set_bold(font_bold)
    # 设置是否斜体
    cur_font.set_italic(font_italic)
    # 设置文字内容
    text_fmt = cur_font.render(text, 1, color)
    # 绘制文字
    screen.blit(text_fmt, pos)
