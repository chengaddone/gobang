"""游戏的工具代码"""
import pygame
from pygame import display
from .functions import mouse_operation, draw_chessboard, draw_mouse_indicator, draw_script


class Game:
    """游戏的工厂类，用该模板生成一个游戏对象"""
    def __init__(self) -> None:
        # 初始化棋盘数组，用于存储本局游戏棋盘上的棋子
        self.chess_arr = []
        # 当前落子标签位，为1时表示当前是黑子，为2时表示为白色
        self.flag = 1
        # 游戏状态变量，为1时表示正在游戏，为2时表示黑子胜利，为3时表示白子胜利
        self.game_state = 1
        # 获取当前的显示屏幕
        self.screen = pygame.display.get_surface()
        # 初始化游戏的帧率控制器
        self.clock = pygame.time.Clock()

    def run(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    exit()
                elif event.type == pygame.MOUSEBUTTONUP:
                    # 鼠标点击事件
                    self.game_state, self.flag = mouse_operation(
                        self.game_state, self.chess_arr, self.flag)
                elif event.type == pygame.KEYDOWN and self.game_state != 1:
                    # 游戏结束时，按空格键可以开始新的游戏
                    if event.key == pygame.K_SPACE:
                        self.flag = 1
                        self.game_state = 1
                        self.chess_arr = list()
            # 画布上色
            self.screen.fill((206, 163, 134))
            # 绘制游戏界面中的线条
            draw_chessboard(self.screen, (30, 35, 12), self.chess_arr)
            # 绘制游戏中界面中的文字
            draw_script(self.flag, self.screen, self.game_state)
            # 绘制鼠标指针
            draw_mouse_indicator(self.screen, self.game_state)
            pygame.display.update()
            self.clock.tick(60)



